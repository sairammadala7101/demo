import React, { useState , useEffect} from 'react';
import axios from 'axios';
// import 'bootstrap/dist/css/bootstrap.css';
// import 'bootstrap/js/dist/modal';
// import 'bootstrap/dist/js/bootstrap.bundle';
const Contact = () =>  {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [data, setData] = useState(null);

  useEffect(() => {
    if(data!=null){
      alert(`Form submitted successfully! ${data.name}`);
      setName('');
      setEmail('');
      setMessage('');
    }
  }, [data]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if(!re.test(email)){
      document.getElementById('email').focus()
    }
    else{
      try {
        axios.create({
          baseURL: 'http://localhost:8000',
        })
        axios.interceptors.response.use(
          (response) => response,
          (error) => {
            if (error.response && error.response.status === 500) {
              alert('failed to send msg');
            }
            return Promise.reject(error);
          }
        );
        await axios.post('/contact', { name, email, message })
        .then(response => {
          const  temp = response.data
          console.log(temp.name,temp.sentstatus,temp.responsemessage)
          setData({...data, name:temp.name})
        })
      } catch (error) {
        console.error(error);
      }
    }
  };

  return (
    <div class="container3">
        <div class="container4">
    <form>
      <label>Name:
        <input type="text"  value={name} onChange={(e) => setName(e.target.value)}/>
      </label><br/>
      <label>Email:
        <input id='email' type="text"  value={email} onChange={(e) => setEmail(e.target.value)}/>
      </label><br/>
      <label>Message:
        <textarea value={message}  onChange={(e) => setMessage(e.target.value)}/>
      </label><br/>
      <button class="button11 justify-content-center" onClick={handleSubmit}>Submit</button>
    </form>
    </div>
    </div>
  )
};
export default Contact;