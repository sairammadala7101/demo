const express = require('express');
const { MongoClient } = require('mongodb');
const cors = require('cors');
const transporter = require('./config.js');
const dotenv = require('dotenv');
dotenv.config()

const url = 'mongodb://127.0.0.1:27017';
const app = express();
const port = 8000;

app.use(express.json());
app.use(cors());


app.post('/contact', async (req, res) => {
  const { name, email, message } = req.body;
  let responsemessage = 'Mail', sentstatus = false;

  const mailOptions = {
    from: req.body.name,
    to: process.env.email,
    subject: req.body.subject,
    html: `
      <p>You have a new contact request.</p>
      <h3>Contact Details</h3>
      <ul>
        <li>Name: ${req.body.name}</li>
        <li>Email: ${req.body.email}</li>
        <li>Message: ${req.body.message}</li>
      </ul>
    `,
  };

  try {
    await new Promise((resolve, reject) => {
      transporter.sendMail(mailOptions, function (err, info) {
        if (err) {
          sentstatus = false;
          responsemessage = 'Message sending failed. Try again after some time!\n'+err;
          reject(err);
        } else {
          sentstatus = true;
          responsemessage = 'Message sent successfully!';
          resolve();
        }
      });
    });
  } catch (error) {
    sentstatus = false;
    responsemessage = 'Message sending failed. Try again later!\n'+error;
  }

  if(sentstatus)
    {try {
      const client = await MongoClient.connect(url);
      const db = client.db('abacus');

      try {
        await db.createCollection('contact_form');
      } catch (error) {
        await db.collection('contact_form').insertOne({ name, email, message, date:Date() });
        const details = await db.collection('contact_form').findOne({ name });
        console.log('details', details);
      }
      client.close();
      res.json({ name, sentstatus, responsemessage });

      }
      catch (error) {
        console.error(error);
        console.log("'Failed to establish a connection to the database'");
        res.status(500).json({ success: false, message: 'Something went wrong. Try again later' });
      }
    }
    else{
      res.sendStatus(500)
    }


    //sending mail

    
    
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
